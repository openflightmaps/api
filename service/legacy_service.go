package service

import (
	"context"

	"gitlab.com/openflightmaps/api/graph/model"
	"gitlab.com/openflightmaps/db-grpc/ent/proto/legacy"
)

type LegacyClientService interface {
	LegacyInsecureQuery(ctx context.Context, nameOfDatabase string, queryString string) (*model.LegacyDataTable, error)
	LegacySQLStoredProcedure(ctx context.Context, procName string, input []string) (*model.LegacyDataTable, error)
}

type legacyClientService struct {
	lc legacy.ClientServiceClient
}

func NewLegacyClientService(lc legacy.ClientServiceClient) LegacyClientService {
	return &legacyClientService{lc: lc}
}

func (lcs *legacyClientService) LegacyInsecureQuery(ctx context.Context, nameOfDatabase string, queryString string) (*model.LegacyDataTable, error) {
	req := &legacy.InsecureQueryRequest{
		Db:    nameOfDatabase,
		Query: queryString,
	}
	dt, err := lcs.lc.InsecureQuery(ctx, req)
	if err != nil {
		return nil, err
	}
	return toDataTable(dt), nil
}

// USER: LegacySQLStoredProcedure is the resolver for the legacySQLStoredProcedure field.
func (lcs *legacyClientService) LegacySQLStoredProcedure(ctx context.Context, procName string, input []string) (*model.LegacyDataTable, error) {
	req := &legacy.ClientStoredProcedureRequest{
		Name:  procName,
		Input: input,
	}
	dt, err := lcs.lc.StoredProcedure(ctx, req)
	if err != nil {
		return nil, err
	}
	return toDataTable(dt), nil
}

func toDataTable(dt *legacy.DataTable) *model.LegacyDataTable {
	res := model.LegacyDataTable{}
	res.Columns = dt.Columns
	res.Types = dt.Types
	for _, row := range dt.Rows {
		r := make([]*string, 0)
		for _, f := range row.Value {
			if f.Value == nil {
				r = append(r, nil)
			} else {
				r = append(r, &f.Value.Value)
			}
		}
		res.Rows = append(res.Rows, r)
	}
	return &res
}
