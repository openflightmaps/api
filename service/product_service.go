package service

import (
	"context"

	"gitlab.com/openflightmaps/api/graph/model"
	"gitlab.com/openflightmaps/api/repository"
	"gitlab.com/openflightmaps/db-grpc/ent/proto/legacy"
)

const typeNameProduct = "Product"

type ProductService interface {
	ProductsForRegionAndAirac(ctx context.Context, region *model.Region, airac string) ([]*model.Product, error)
}

type productService struct {
	repo repository.ProductRepository
	lc   legacy.ClientServiceClient
}

func NewProductService(repo repository.ProductRepository) ProductService {
	return &productService{repo: repo}
}

func (ps *productService) GetProduct(ctx context.Context, id model.GlobalID) (*model.Product, error) {
	return ps.repo.GetProduct(ctx, id)
}

func (ps *productService) ProductsForRegionAndAirac(ctx context.Context, region *model.Region, airac string) ([]*model.Product, error) {
	return ps.repo.ListProductsForAiracAndRegion(ctx, airac, region)
}
