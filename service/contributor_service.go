package service

import (
	"context"
	"fmt"

	"gitlab.com/openflightmaps/api/graph/model"
	"gitlab.com/openflightmaps/api/repository"
)

const typeNameContributor = "Contributor"

type ContributorService interface {
	Contributors(ctx context.Context) ([]*model.Contributor, error)
	Contributor(ctx context.Context, id model.GlobalID) (*model.Contributor, error)
	ContributorsForRegion(ctx context.Context, obj *model.Region) ([]*model.Contributor, error)
}

type contributorService struct {
	repo repository.ContributorRepository
}

func NewContributorService(repo repository.ContributorRepository) ContributorService {
	return &contributorService{repo: repo}
}

func (cs *contributorService) Contributors(ctx context.Context) ([]*model.Contributor, error) {
	res := make([]*model.Contributor, 0)
	contribs, err := cs.repo.ListContributors(ctx)
	if err != nil {
		return nil, err
	}
	for _, reg := range contribs {
		r := *reg
		r.ID = model.NewGlobalID(typeNameContributor, r.InternalID)
		res = append(res, &r)
	}
	return res, nil
}

func (cs *contributorService) Contributor(ctx context.Context, id model.GlobalID) (*model.Contributor, error) {
	contributors, err := cs.Contributors(ctx)
	if err != nil {
		return nil, err
	}

	for _, c := range contributors {
		if c.ID == id {
			return c, err
		}
	}
	return nil, fmt.Errorf("not found")
}

func (cs *contributorService) ContributorsForRegion(ctx context.Context, obj *model.Region) ([]*model.Contributor, error) {
	res := make([]*model.Contributor, 0)
	cont, err := cs.Contributors(ctx)
	if err != nil {
		return nil, err
	}
	for _, c := range cont {
		for _, cr := range c.Regions {
			if cr.InternalID == obj.InternalID {
				res = append(res, c)
			}
		}
	}
	return res, nil
}
