package service

import (
	"context"

	"gitlab.com/openflightmaps/api/graph/model"
	"gitlab.com/openflightmaps/api/repository"
)

const typeNameUser = "User"

type UserService interface {
	User(ctx context.Context, id model.GlobalID) (*model.User, error)
}

type userService struct {
	repo repository.UserRepository
}

func NewUserService(repo repository.UserRepository) UserService {
	return &userService{repo: repo}
}

func (us *userService) User(ctx context.Context, id model.GlobalID) (*model.User, error) {
	u, err := id.StringValue(typeNameUser)
	if err != nil {
		return nil, err
	}

	user, err := us.repo.GetUser(ctx, u)
	if err != nil {
		return nil, err
	}

	res := model.User{
		ID:         id,
		InternalID: u,
		Username:   user.Username,
	}
	return &res, err
}
