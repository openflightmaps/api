package service

import (
	"context"
	"fmt"

	"gitlab.com/openflightmaps/api/repository"
)

type AuthService interface {
	ValidateAndGetUserID(ctx context.Context, username, password string) (string, error)
	GetUserByID(ctx context.Context, id string) (userId string, name string, isAdmin bool, err error)
}

type authService struct {
	ur repository.UserRepository
}

func NewAuthService(ur repository.UserRepository) AuthService {
	return &authService{
		ur: ur,
	}
}

// TODO: these function uses the legacy StoredProcedure functions, migrate
func (as *authService) ValidateAndGetUserID(ctx context.Context, username, password string) (string, error) {
	return as.ur.ValidateAndGetUserID(ctx, username, password)
}

func (as *authService) GetUserByID(ctx context.Context, id string) (string, string, bool, error) {
	user, err := as.ur.GetUser(ctx, id)
	if err != nil {
		return "", "", false, err
	}
	userId := fmt.Sprintf("legacy-%s", user.InternalID)
	return userId, user.Username, user.InternalID == "0", nil
}
