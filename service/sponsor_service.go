package service

import (
	"context"
	"fmt"

	"gitlab.com/openflightmaps/api/graph/model"
	"gitlab.com/openflightmaps/api/repository"
)

const typeNameSponsor = "Sponsor"

type SponsorService interface {
	Sponsor(ctx context.Context, id model.GlobalID) (*model.Sponsor, error)
	Sponsors(ctx context.Context) ([]*model.Sponsor, error)
}

type sponsorService struct {
	repo repository.SponsorRepository
}

func NewSponsorService(repo repository.SponsorRepository) SponsorService {
	return &sponsorService{repo: repo}
}

func (ss *sponsorService) Sponsor(ctx context.Context, id model.GlobalID) (*model.Sponsor, error) {
	sponsors, err := ss.Sponsors(ctx)
	if err != nil {
		return nil, err
	}

	for _, r := range sponsors {
		if r.ID == id {
			return r, err
		}
	}
	return nil, fmt.Errorf("not found")
}

func (ss *sponsorService) Sponsors(ctx context.Context) ([]*model.Sponsor, error) {
	sponsors, err := ss.repo.ListSponsors(ctx)
	if err != nil {
		return nil, err
	}

	for _, s := range sponsors {
		s.ID = model.NewGlobalID(typeNameSponsor, s.InternalID)
	}

	return sponsors, nil
}
