package service

import (
	"context"
	"fmt"

	"gitlab.com/openflightmaps/api/graph/model"
	"gitlab.com/openflightmaps/api/repository"
)

const typeNameRegion = "Region"

type RegionService interface {
	Regions(ctx context.Context, unreleased bool) ([]*model.Region, error)
	Region(ctx context.Context, id model.GlobalID) (*model.Region, error)
	RegionsForContributor(ctx context.Context, obj *model.Contributor) ([]*model.Region, error)
}

type regionService struct {
	repo repository.RegionRepository
}

func NewRegionService(repo repository.RegionRepository) RegionService {
	return &regionService{repo: repo}
}

func (rs *regionService) Regions(ctx context.Context, unreleased bool) ([]*model.Region, error) {
	res := make([]*model.Region, 0)
	regions, err := rs.repo.ListRegions(ctx)
	if err != nil {
		return nil, err
	}
	for _, reg := range regions {
		r := *reg
		if r.Status != model.RegionStatusReleased {
			if unreleased {
				// ok, show unrelased
			} else {
				continue
			}
		}
		r.ID = model.NewGlobalID(typeNameRegion, r.InternalID)
		res = append(res, &r)
	}
	return res, nil
}

func (rs *regionService) Region(ctx context.Context, id model.GlobalID) (*model.Region, error) {
	regs, err := rs.Regions(ctx, true)
	if err != nil {
		return nil, err
	}

	for _, r := range regs {
		if r.ID == id {
			return r, err
		}
	}
	return nil, fmt.Errorf("not found")
}

func (rs *regionService) RegionsForContributor(ctx context.Context, obj *model.Contributor) ([]*model.Region, error) {
	regs := make([]*model.Region, 0)
	for _, rn := range obj.Regions {
		if rn == nil {
			continue
		}
		rns := model.NewGlobalID("Region", *&rn.InternalID)
		reg, err := rs.Region(ctx, rns)
		if err != nil {
			return nil, err
		}
		regs = append(regs, reg)
	}
	return regs, nil
}
