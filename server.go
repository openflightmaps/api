package main

import (
	"log"
	"net/http"
	"os"

	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/99designs/gqlgen/graphql/handler/debug"
	"github.com/99designs/gqlgen/graphql/playground"
	"github.com/gorilla/mux"
	"gitlab.com/openflightmaps/api/graph"
	"gitlab.com/openflightmaps/api/graph/generated"
	"gitlab.com/openflightmaps/api/internal"
	"gitlab.com/openflightmaps/api/middleware/auth"
	"gitlab.com/openflightmaps/api/repository"
	"gitlab.com/openflightmaps/api/service"
	"gitlab.com/openflightmaps/db-grpc/ent/proto/entpb"
	"gitlab.com/openflightmaps/db-grpc/ent/proto/legacy"
	"github.com/klauspost/compress/gzhttp"
	"google.golang.org/grpc"
)

const defaultPort = "8080"

func main() {
	// Load config.yaml
	config, err := internal.LoadConfig()
	if err != nil {
		panic(err)
	}

	port := os.Getenv("PORT")
	if port == "" {
		port = defaultPort
	}

	// TODO, move to service
	// Open a connection to the server.
	size := 1024 * 1024 * 32

	// connection to ofm DB backend service
	conn, err := grpc.Dial(":5000", grpc.WithInsecure(), grpc.WithDefaultCallOptions(grpc.MaxCallRecvMsgSize(size)))
	if err != nil {
		log.Fatalf("failed connecting to server: %s", err)
	}
	defer conn.Close()

	// connection to ofmServices DB backend service
	connServices, err := grpc.Dial(":5001", grpc.WithInsecure(), grpc.WithDefaultCallOptions(grpc.MaxCallRecvMsgSize(size)))
	if err != nil {
		log.Fatalf("failed connecting to server: %s", err)
	}
	defer connServices.Close()

	// Create a UserService Client service on the connection.
	uc := entpb.NewUserServiceClient(conn)

	// Create a Legacy Client service on the connection.
	lc := legacy.NewClientServiceClient(conn)

	// set up user repositories, TODO: make it possible to switch them on and off
	uro := repository.NewOfmDBUserRepository(uc, lc)
	urs := repository.NewStaticUserRepository(config)
	ur := repository.NewMergedUserRepository(urs, uro)

	// Setup region and contributor repository using config data
	rr := repository.NewRegionRepository(config)
	cr := repository.NewContributorRepository(config)
	pr := repository.NewProductRepository(config)
	sr := repository.NewSponsorRepository(config)

	// Create a region service instance
	rs := service.NewRegionService(rr)

	// Create contributor service
	cs := service.NewContributorService(cr)

	// Create legacy client service
	lcs := service.NewLegacyClientService(lc)

	// Create user service
	us := service.NewUserService(ur)

	// Create product service
	ps := service.NewProductService(pr)

	// Create sponsor service
	ss := service.NewSponsorService(sr)

	// Create auth service
	as := service.NewAuthService(ur)

	// Set up graphql
	router := mux.NewRouter()
	router.Use(auth.Middleware(as))

	// create resolver
	resolver := graph.NewResolver(rs, cs, lcs, us, ps, ss)

	c := generated.Config{Resolvers: resolver}

	// set up auth directive
	c.Directives.HasRole = auth.HasRole

	srv := handler.NewDefaultServer(generated.NewExecutableSchema(c))

	if config.Debug {
		srv.Use(&debug.Tracer{})
	}

	router.Handle("/", playground.Handler("GraphQL playground", "/graphql"))
	//router.Handle("/graphql", srv)
	router.Handle("/graphql", gzhttp.GzipHandler(srv))

	log.Printf("connect to http://localhost:%s/ for GraphQL playground", port)
	log.Fatal(http.ListenAndServe(":"+port, router))
}
