package auth

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/99designs/gqlgen/graphql"
	"github.com/patrickmn/go-cache"
	"gitlab.com/openflightmaps/api/graph/model"
	"gitlab.com/openflightmaps/api/service"
)

// A private key for context that only this package can access. This is important
// to prevent collisions between different context uses
var userCtxKey = &contextKey{"user"}

type contextKey struct {
	name string
}

// A stand-in for our database backed user object
type User struct {
	ID      string
	Name    string
	IsAdmin bool
}

// Middleware decodes the share session cookie and packs the session into context
func Middleware(as service.AuthService) func(http.Handler) http.Handler {
	cache := cache.New(5*time.Minute, 10*time.Minute)

	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			username, password, ok := r.BasicAuth()

			// Allow unauthenticated users in
			if !ok {
				next.ServeHTTP(w, r)
				return
			}

			ctx := r.Context()

			var user *User

			// check cached auth:
			cacheKey := fmt.Sprintf("%s-%s", username, password)
			cached, found := cache.Get(cacheKey)
			if found {
				user = cached.(*User)
			} else {
				userId, err := as.ValidateAndGetUserID(ctx, username, password)
				if err != nil {
					// TODO, remove password logging
					log.Printf("ValidateAndGetUserID failed with error for user %s, password %s, %v", username, password, err)
					http.Error(w, "Invalid auth header", http.StatusForbidden)
					return
				}

				// get the user from the database
				user = &User{}
				user.ID, user.Name, user.IsAdmin, err = as.GetUserByID(ctx, userId)
				if err != nil {
					log.Println("GetUserByID failed with error", err)
					http.Error(w, "Invalid auth header", http.StatusForbidden)
					return
				}
				log.Println(user)
				// cache user auth
				cache.Set(cacheKey, user, 0)
			}
			// put it in context
			ctx = context.WithValue(ctx, userCtxKey, user)

			// and call the next with our new context
			r = r.WithContext(ctx)
			next.ServeHTTP(w, r)
		})
	}
}

// ForContext finds the user from the context. REQUIRES Middleware to have run.
func ForContext(ctx context.Context) *User {
	raw, _ := ctx.Value(userCtxKey).(*User)
	return raw
}

func HasRole(ctx context.Context, obj interface{}, next graphql.Resolver, role model.Role) (interface{}, error) {
	if !ForContext(ctx).HasRole(role) {
		// block calling the next resolver
		return nil, fmt.Errorf("access denied")
	}

	// or let it pass through
	return next(ctx)
}

func (u *User) HasRole(role model.Role) bool {
	if u == nil {
		return false
	}
	if role == model.RoleUser {
		return true
	}
	if role == model.RoleAdmin && u.IsAdmin {
		return true
	}
	return false
}
