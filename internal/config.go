package internal

import (
	"fmt"
	"log"

	"github.com/spf13/viper"
	"gitlab.com/openflightmaps/api/graph/model"
)

type AuthConfig struct {
	OfmDb struct {
		Enabled bool
	}
	Static struct {
		Enabled bool
		Users   []*model.User
	}
}
type Config struct {
	Auth AuthConfig
	// additional config files to include
	ExtraConfig  []string
	Regions      []*model.Region
	Contributors []*model.Contributor
	Sponsors     []*model.Sponsor
	Debug        bool
}

func LoadConfig() (*Config, error) {
	// load config.yaml from local dir or config subdir
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	viper.AddConfigPath("/config")
	viper.AddConfigPath("config")
	viper.AddConfigPath(".")

	viper.SetDefault("DB.Port", 3306)

	if err := viper.ReadInConfig(); err != nil {
		return nil, fmt.Errorf("failed to load config: %v", err)
	}

	// load additional config files, e.g. regions, contributors (without extension)
	for _, c := range viper.GetStringSlice("extraConfig") {
		log.Printf("loading extra config %s", c)
		viper.SetConfigName(c)

		if err := viper.MergeInConfig(); err != nil {
			return nil, fmt.Errorf("failed to load extra config: %v", err)
		}
	}

	cfg := Config{}
	if err := viper.Unmarshal(&cfg); err != nil {
		return nil, fmt.Errorf("invalid config: %v", err)
	}

	return &cfg, nil
}
