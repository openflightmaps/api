package repository

import (
	"context"

	"gitlab.com/openflightmaps/api/graph/model"
	"gitlab.com/openflightmaps/api/internal"
)

type SponsorRepository interface {
	ListSponsors(ctx context.Context) ([]*model.Sponsor, error)
}

type sponsorRepository struct {
	config *internal.Config
}

func NewSponsorRepository(config *internal.Config) SponsorRepository {
	return &sponsorRepository{
		config: config,
	}
}

func (rr *sponsorRepository) ListSponsors(ctx context.Context) ([]*model.Sponsor, error) {
	return rr.config.Sponsors, nil
}
