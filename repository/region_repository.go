package repository

import (
	"context"

	"gitlab.com/openflightmaps/api/graph/model"
	"gitlab.com/openflightmaps/api/internal"
)

type RegionRepository interface {
	ListRegions(ctx context.Context) ([]*model.Region, error)
}

type regionRepository struct {
	config *internal.Config
}

func NewRegionRepository(config *internal.Config) RegionRepository {
	return &regionRepository{
		config: config,
	}
}

func (rr *regionRepository) ListRegions(ctx context.Context) ([]*model.Region, error) {
	return rr.config.Regions, nil
}
