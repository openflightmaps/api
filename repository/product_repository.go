package repository

import (
	"context"
	"fmt"

	"gitlab.com/openflightmaps/api/graph/model"
	"gitlab.com/openflightmaps/api/internal"
)

type ProductRepository interface {
	GetProduct(ctx context.Context, id model.GlobalID) (*model.Product, error)
	ListProducts(ctx context.Context) ([]*model.Product, error)
	ListProductsForAirac(ctx context.Context, airac string) ([]*model.Product, error)
	ListProductsForAiracAndRegion(ctx context.Context, airac string, region *model.Region) ([]*model.Product, error)
}

type productRepository struct {
	config *internal.Config
}

func NewProductRepository(config *internal.Config) ProductRepository {
	return &productRepository{
		config: config,
	}
}

// TODO: this should be cached fully in-mem, product list is in the order of a few thousands
func (rr *productRepository) ListProducts(ctx context.Context) ([]*model.Product, error) {
	return []*model.Product{
		{ID: "bla", Format: model.ProductFormatOfmx, Variant: model.ProductVariantNormal, Region: &model.Region{InternalID: "ED", ShortName: "ED"}, Airac: "2211"},
	}, nil
}

func (rr *productRepository) GetProduct(ctx context.Context, id model.GlobalID) (*model.Product, error) {
	pa, err := rr.ListProducts(ctx)
	if err != nil {
		return nil, err
	}
	for _, p := range pa {
		if p.ID == id {
			return p, nil
		}
	}
	return nil, fmt.Errorf("not found")
}

// TODO: this should be cached
func (rr *productRepository) ListProductsForAirac(ctx context.Context, airac string) ([]*model.Product, error) {
	res := make([]*model.Product, 0)
	pa, err := rr.ListProducts(ctx)
	if err != nil {
		return nil, err
	}
	for _, p := range pa {
		if p.Airac == airac {
			res = append(res, p)
		}
	}
	return res, nil
}

func (rr *productRepository) ListProductsForAiracAndRegion(ctx context.Context, airac string, region *model.Region) ([]*model.Product, error) {
	res := make([]*model.Product, 0)
	pa, err := rr.ListProductsForAirac(ctx, airac)
	if err != nil {
		return nil, err
	}
	for _, p := range pa {
		if p.Region != nil && p.Region.InternalID == region.InternalID {
			res = append(res, p)
		}
	}
	return res, nil
}
