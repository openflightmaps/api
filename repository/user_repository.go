package repository

import (
	"context"
	"errors"
	"log"
	"strconv"

	"gitlab.com/openflightmaps/api/graph/model"
	"gitlab.com/openflightmaps/api/internal"
	"gitlab.com/openflightmaps/db-grpc/ent/proto/entpb"
	"gitlab.com/openflightmaps/db-grpc/ent/proto/legacy"
)

var UserNotFoundError = errors.New("UserNotFound")

type UserRepository interface {
	GetUser(ctx context.Context, id string) (*model.User, error)
	ValidateAndGetUserID(ctx context.Context, username, password string) (string, error)
}

type mergedUserRepository struct {
	urs []UserRepository
}

func NewMergedUserRepository(urs ...UserRepository) UserRepository {
	return &mergedUserRepository{
		urs: urs,
	}
}

// TODO: these function uses the legacy StoredProcedure functions, migrate
func (mur *mergedUserRepository) ValidateAndGetUserID(ctx context.Context, username, password string) (string, error) {
	for _, ur := range mur.urs {
		u, err := ur.ValidateAndGetUserID(ctx, username, password)
		if errors.Is(err, UserNotFoundError) {
			// fine, continue with next repo
			continue
		}
		return u, err
	}
	return "", UserNotFoundError
}

func (mur *mergedUserRepository) GetUser(ctx context.Context, id string) (*model.User, error) {
	for _, ur := range mur.urs {
		user, err := ur.GetUser(ctx, id)

		if errors.Is(err, UserNotFoundError) {
			// fine, continue with next repo
			continue
		}
		return user, err
	}
	return nil, UserNotFoundError
}

type staticUserRepository struct {
	authConfig *internal.AuthConfig
}

func NewStaticUserRepository(config *internal.Config) UserRepository {
	return &staticUserRepository{
		authConfig: &config.Auth,
	}
}

func (ur *staticUserRepository) ValidateAndGetUserID(ctx context.Context, username, password string) (string, error) {
	log.Println("STATIC")
	for _, u := range ur.authConfig.Static.Users {
		log.Println(u)
		if u.Username == username && u.Password == password {
			log.Println("STATIC success")
			return u.InternalID, nil
		}
	}
	return "", UserNotFoundError
}

func (ur *staticUserRepository) GetUser(ctx context.Context, id string) (*model.User, error) {
	for _, user := range ur.authConfig.Static.Users {
		if user.InternalID == id {
			u := *user
			return &u, nil
		}
	}
	return nil, UserNotFoundError
}

type ofmDBUserRepository struct {
	config *internal.Config
	uc     entpb.UserServiceClient
	lc     legacy.ClientServiceClient
}

func NewOfmDBUserRepository(uc entpb.UserServiceClient, lc legacy.ClientServiceClient) UserRepository {
	return &ofmDBUserRepository{
		uc: uc,
		lc: lc,
	}
}

func (ur *ofmDBUserRepository) ValidateAndGetUserID(ctx context.Context, username, password string) (string, error) {
	log.Println("OFMDB")
	orgId := "-1"
	req := legacy.ClientStoredProcedureRequest{
		Name:  "QueryTable",
		Input: []string{username, password, "U1T", orgId, "-1", "-1"},
	}
	users, err := ur.lc.StoredProcedure(ctx, &req)
	if err != nil {
		return "", err
	}
	if len(users.Rows) != 1 {
		log.Println("login failed")
		return "", UserNotFoundError
	}
	userId := users.Rows[0].GetValue()[0].GetValue().Value
	return userId, err
}

func (ur *ofmDBUserRepository) GetUser(ctx context.Context, id string) (*model.User, error) {
	uid, err := strconv.ParseInt(id, 10, 64)
	if err != nil {
		return nil, err
	}
	req := entpb.GetUserRequest{
		Id: uid,
	}
	user, err := ur.uc.Get(ctx, &req)
	res := model.User{
		InternalID: id,
		Username:   user.Username,
	}
	return &res, err
}
