package repository

import (
	"context"

	"gitlab.com/openflightmaps/api/graph/model"
	"gitlab.com/openflightmaps/api/internal"
)

type ContributorRepository interface {
	ListContributors(ctx context.Context) ([]*model.Contributor, error)
}

type contributorRepository struct {
	config *internal.Config
}

func NewContributorRepository(config *internal.Config) ContributorRepository {
	return &contributorRepository{
		config: config,
	}
}

func (rr *contributorRepository) ListContributors(ctx context.Context) ([]*model.Contributor, error) {
	return rr.config.Contributors, nil
}
