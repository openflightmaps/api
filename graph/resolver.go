//go:generate go run github.com/99designs/gqlgen generate
package graph

import (
	"gitlab.com/openflightmaps/api/service"
)

type Resolver struct {
	rs  service.RegionService
	cs  service.ContributorService
	lcs service.LegacyClientService
	us  service.UserService
	ps  service.ProductService
	ss  service.SponsorService
}

func NewResolver(rs service.RegionService, cs service.ContributorService, lcs service.LegacyClientService, us service.UserService, ps service.ProductService, ss service.SponsorService) *Resolver {
	return &Resolver{
		rs:  rs,
		cs:  cs,
		lcs: lcs,
		us:  us,
		ps:  ps,
		ss:  ss,
	}
}
