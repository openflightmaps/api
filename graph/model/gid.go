package model

import (
	"encoding/base64"
	"fmt"
	"strconv"
	"strings"
)

const version = "01"

type GlobalID string

func (g GlobalID) decode() (string, string, string, error) {
	s := string(g)
	d, err := base64.StdEncoding.DecodeString(s)
	if err != nil {
		return "", "", "", err
	}
	x := strings.Split(string(d), ":")
	if len(x) != 3 {
		return "", "", "", fmt.Errorf("invalid GlobalID")
	}

	return x[0], x[1], x[2], nil
}
func encode(typeName, value string) GlobalID {
	return GlobalID(base64.StdEncoding.EncodeToString([]byte(fmt.Sprintf("%s:%s:%s", version, typeName, value))))
}

func (g GlobalID) value(typeName string) (string, error) {
	_, t, v, err := g.decode()

	if err != nil {
		return "", err
	}

	if t != typeName {
		return "", fmt.Errorf("invalid GlobalID")
	}
	return v, err
}

func (g GlobalID) TypeName() (string, error) {
	_, t, _, err := g.decode()
	return t, err
}

func (g GlobalID) IntValue(typeName string) (int64, error) {
	v, err := g.value(typeName)

	if err != nil {
		return 0, err
	}

	i, err := strconv.ParseInt(v, 10, 64)
	return i, err
}

func (g GlobalID) StringValue(typeName string) (string, error) {
	return g.value(typeName)
}

func NewGlobalID(typeName, value string) GlobalID {
	return encode(typeName, value)
}
