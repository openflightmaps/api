package model

import (
	"strconv"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestEncode(t *testing.T) {
	typeName := "MyType"
	iValue := int64(1234567890)
	sValue := strconv.FormatInt(iValue, 10)
	x := NewGlobalID(typeName, sValue)
	decType, err := x.TypeName()
	decStringValue, err := x.StringValue(typeName)
	decIntValue, err := x.IntValue(typeName)

	assert.Nil(t, err, "error occured")
	assert.Equal(t, "MDE6TXlUeXBlOjEyMzQ1Njc4OTA=", string(x), "encoding error")
	assert.Equal(t, typeName, decType, "type mismatch")
	assert.Equal(t, sValue, decStringValue, "string value mismatch")
	assert.Equal(t, iValue, decIntValue, "int value mismatch")
}
